export default {
	addEvent(elm, evType, fn, useCapture) {
		useCapture = useCapture || false;
		if (elm.addEventListener) {
			elm.addEventListener(evType, fn, false);
			return true;
		} else if (elm.attachEvent) {
			var r = elm.attachEvent('on' + evType, fn);
			return r;
		} else {
			elm['on' + evType] = fn;
		}
	},

	removeEvent(elm, evType, fn, useCapture) {
		useCapture = useCapture || false;
		if (elm.removeEventListener){
			elm.removeEventListener(evType, fn, false);
		} else if (elm.detachEvent) {
			 elm.detachEvent("on"+evType, fn);
		} else {
			delete elm["on"+evType];
		}
	}
}