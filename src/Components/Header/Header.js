import React from 'react';
import styles from './Header.css';

let HeaderComponent = React.createClass({
    render: function () {
        return <div className={styles.Header}>
            <div className={styles.text}>{this.props.title}</div>
        </div>;
    }
});

export default HeaderComponent;
