import styles from './Testimonials.css';
import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import Helpers from "../../../utils/Helpers";
import Header from '../../../Components/Header';

let getNextSlideValue = (present, total) => {
	let k = present + 1;

	if (k === total) {
		k = 0;
	};

	return k;
};

let getPreviousSlideValue = (present, total) => {
	let k = present - 1;

	if (k < 0) {
		k = total - 1;
	}

	return k
}

let Slide = React.createClass({
	getInitialState(){
		return {
			left: this.props.left || "0px",
			zIndex: this.props.zIndex || 0,
			display: this.props.display || "none"
		}
	},
	componentWillReceiveProps(props){
		this.setState({
			left: "left" in props ? props.left : this.state.left,
			zIndex: "zIndex" in props ? props.zIndex : this.state.zIndex,
			display: "display" in props ? props.display : this.state.display
		});
	},
	render() {
		return <div className={styles["slide"]} style={{
			left: this.state.left,
			display: this.state.display,
			zIndex: this.state.zIndex
		}}><p>{this.props.text}</p></div>;
	}
});

let Carousel = React.createClass({
	getInitialState(){
		return {
			active: 0,
			moveType: null,
			previous: null,
			slides: [],
			count: 0,
			width: "100%",
			height: 0,
			animationEnded: true,
			autoPlayTimer: null
		}
	},
	// componentWillMount(){
	// 	this.resizeHandler();
	// },
	componentDidMount(){
		let children = this.props.children;
		if(!Array.isArray(children)) {
			children = [children]
		}

		let slides = children.map(v => {
			if (v.type === "p") {
				return {
					text: v.props.children
				};
			}
		});

		this.setState({
			count: slides.length,
			slides: slides,
			width: ReactDOM.findDOMNode(this).offsetWidth,
			height: (this.props.height * this.props.width) / ReactDOM.findDOMNode(this).offsetWidth
		});

		Helpers.addEvent(window, "resize", this.resizeHandler);

		if (this.props.autoplay) {
			this.autoPlay();
		}
	},
	componentWillUnmount () {
		Helpers.removeEvent(window, "resize", this.resizeHandler);
		if (this.state.autoPlayTimer) {
			clearInterval(this.state.autoPlayTimer);
		}
	},
	resizeHandler(){
		let width = ReactDOM.findDOMNode(this).offsetWidth;

		this.setState({
			width: width,
			height: (this.props.height * this.props.width) / width
		});
	},
	showPrevious(e){
		e && e.preventDefault();

		this.setState({
			moveType: "prev",
			previous: this.state.active,
			animationEnded: false,
			active: getPreviousSlideValue(this.state.active, this.state.count)
		})
	},
	showNext(e){
		e && e.preventDefault();

		this.setState({
			moveType: "next",
			previous: this.state.active,
			animationEnded: false,
			active: getNextSlideValue(this.state.active, this.state.count)
		})
	},
	goToSlide(goto){
		if (goto !== this.state.active) {
			this.setState({
				animationEnded: false,
				moveType: "goto",
				previous: this.state.active,
				active: goto
			});
		}
	},
	_getElementHeight(id){
		return ReactDOM.findDOMNode(this.refs['slide_' + id]).offsetHeight || this.state.height
	},
	onTransitionEnd(){
		this.setState({
			animationEnded: true,
			height: ReactDOM.findDOMNode(this.refs['slide_' + this.state.active]).offsetHeight
		})
		if (this.props.autoplay) {
			this.autoPlay();
		}
	},
	play: function(){
		this.setState({
			moveType: "next",
			previous: this.state.active,
			animationEnded: false,
			active: getNextSlideValue(this.state.active, this.state.count)
		});
	},
	autoPlay: function () {
		if (this.state.autoPlayTimer) {
			clearTimeout(this.state.autoPlayTimer);
		}
		if (this.props.autoplay) {
			this.setState({
				autoPlayTimer: setTimeout(this.play, this.props.autoplaySpeed)
			});
		}
	},
	render() {
		let prev = getPreviousSlideValue(this.state.active, this.state.count)
			,next = getNextSlideValue(this.state.active,this.state.count)
			,control_styles = {left: "0px", width: "100%", height: this.state.height + "px"};

		if (this.state.moveType !== null && this.state.animationEnded !== true) {
			control_styles.transform = "translateX(" + (this.state.previous < this.state.active ? "-" : "") + this.state.width + "px)";
			control_styles.transitionDuration = "450ms";
		}

		return <div className={styles.Carousel} ref="carousel_container">
			<div className={styles["slides-container"]} ref="carousel_slides_container" style={{width: "100%", height: this.state.height + "px"}}>
				<div className={styles["slides-control"]} style={control_styles} onTransitionEnd={this.onTransitionEnd}>
					{
						this.state.slides.map((v, k) => {
							let style = {};

							if (this.state.moveType === null) {
								if (k === next) {
									style.display = "block";
									style.left = this.state.width
								} else if (k === prev) {
									style.display = "block";
									style.left =  (-1 * this.state.width)
								} else if (k === this.state.active) {
									style.display = "block";
									style.zIndex = 10;
								}
							} else if (!this.state.animationEnded) {
								if (k === this.state.active) {
									style.left = (this.state.previous < this.state.active ? 1 : -1) * this.state.width + "px";
									style.zIndex = 10;
									style.display = "block";
								}

								if (this.state.moveType === "goto" && k !== this.state.active && k !== this.state.previous) {
									style.left = "0px";
									style.zIndex = 0;
									style.display = "none";
								}
							} else {
								if (k === this.state.active) {
									style.left = "0px";
								} else {
									style.left = "0px";
									style.zIndex = 0;
									style.display = "none";
								}
							}

							return <Slide key={k} ref={"slide_" + k} text={v.text} {...style}/>
						})
					}
				</div>
			</div>
			<ul className={styles["pagination"]}>
				{
					this.state.slides.map((v, k) => {
						return <li className={styles["item"]} key={k}>
							<a href="#" className={cx(styles["atag"], {[styles["active"]]: k === this.state.active})} onClick={(e)=>{e.preventDefault();this.goToSlide(k);}}>1</a>
						</li>
					})
				}
			</ul>
		</div>;
	}
});

Carousel.defaultProps = {
  autoplay: false,
  autoplaySpeed: 5000,
  width: 940,
  height: 350
}


let testCarusel = React.createClass({
	render() {
		let {currentSlide, slideCount, ...props} = this.props;

		return <div className={styles.reviews}>
			<Header title="REVIEWS"/>
			<Carousel  width="940" height="240">
			<p>"From bottom of my heart I feel glad to be a part in this prestigious project.The trainers handled classes from morning 9am-5pm is extraordinary.They make each and every participant to involve in sessions.Thank you very much APSCHE and BRITISH COUNCIL for your effort to make this project grand success.Surely, I will take this project as a challenge to make your dreams come true."
				<br></br>"It is an excellent project aimed to equip teachers with essential skills thereby improving the quality of education."
			</p>
			<p>"Excellent . Really I enjoyed and learnt a lot.Previously I had attended several training programs in my in service of 20 years.I have been acting as a trainer for 10 years also. I can say this is the best and unique one among all of them as it has clarity and exact execution of training and its objective to improve learners' communication skills and employment skills which I feel the most important aspect of education. I hope students in AP would be beneficial by cascading this project successfully to the root level."
				<br></br>"The project provides a wonderful platform to the learners of English language, if it is implemented properly as it is designed. The success rate of the project depends on the teachers who teaches the students. If they take it forward effectively, it is will really be useful."</p>
			<p>"Really, it is a great effort of APSCHE. It not only helps the students but also improves the proficiency of English Teachers in terms of training skills. We are all extremely happy about the British Council and APSCHE's communication skills project."
				<br></br>"It's worth useful. it would be much effective when the teachers too like us will have the same zeal of taking it to the real classroom environment implementing the same."</p>
		</Carousel>
		</div>;
	}
});

export default testCarusel;