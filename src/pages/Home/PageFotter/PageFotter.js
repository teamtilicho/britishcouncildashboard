import React from 'react';
import ReactDOM from 'react-dom';
import styles from './PageFotter.css';
import cx from 'classnames';

var PageFotter = React.createClass({

    render: function () {
        return <div className={cx(styles.footer, 'cf')}>
            <div className={styles["first"]}>© 2017 Tilicho Labs . All Rights Reserved.</div>
            <div className={styles["second"]}> powered by &nbsp; <a href="http://tilicho.in/" target="blank">
                <img alt="TilichoLabs" src="assets/logo.png"/>
            </a>
            </div>
        </div>;
    }
});

export default PageFotter;