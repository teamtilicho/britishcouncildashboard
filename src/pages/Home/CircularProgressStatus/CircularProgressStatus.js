import React from 'react';
import Header from '../../../Components/Header';
import ProgressBar from './ProgressBar';
import styles from './CircularProgressStatus.css';
import cx from 'classnames';

let CircleRepresentation = React.createClass({
	render: function() {
		let percentage = (this.props.info.registered/this.props.info.total) * 100;
		return <div>
			<ProgressBar percentage={percentage} classForPercentage={styles.progressbar}/>
			<p>{this.props.info.registered + "/" + this.props.info.total}</p>
			<p>{this.props.info.text}</p>
		</div>;
	}
});

var SecondComponent = React.createClass({

	getInitialState : function() {

		var studentsInfo = {
				total : 100000,
				registered: 82000,
				text: "Students identified in colleges across the state",
				id: 1
			},
			teachersInfo = {
				total : 2500,
				registered: 1900,
				text: "Teachers identified in colleges across the state",
				id: 2
			},
			mastersInfo = {
				total : 114,
				registered: 114,
				text: "Master trainers trained in SPMVV, Tirupati & ANU, Guntur",
				id: 3
			};

		return {
			studentsInfo,
			teachersInfo,
			mastersInfo
		};
	},

	render:function() {
		return (
			<div className={cx(styles.CircularStatus, 'cf')}>
				<Header title="STATS"/>
				<div className={styles.cell}>
					<CircleRepresentation info={this.state.studentsInfo}/>
				</div>
				<div className={styles.cell}>
					<CircleRepresentation info={this.state.teachersInfo}/>
				</div>
				<div className={styles.cell}>
					<CircleRepresentation info={this.state.mastersInfo} />
				</div>
			</div>
		);
	}
});

export default SecondComponent;