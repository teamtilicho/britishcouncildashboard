import styles from './FeedBack.css';
import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import Header from '../../../Components/Header';
import cx from 'classnames';

var StarsComponent = React.createClass({

	render: function () {console.log(this.props.percentage * 5/100);
		return (
			<div>
				{/*<StarRatingComponent
					name={this.props.name}
					count = {this.props.starCount}
					value = {this.props.starCount}
					size={50}
					color2={'#ffd700'}
					editing={false}
				/>*/}
				<StarRatingComponent
					name={this.props.name}
					starColor="#ffb400"
					emptyStarColor="#ffb400"
					editing={false}
					starCount={5}
					value={this.props.starCount}
					renderStarIcon={(index, value) => {
					  return <span className={index <= value ? styles['star'] : cx(styles['star'],styles['empty'])} />;
					}}
					renderStarIconHalf={() => <span className={cx(styles["star"], styles["half_empty"])} />}
				  />
				<p className={styles["RatingLabel"]}> {this.props.text} </p>
			</div>
		);
	}
});

var FeedBackComponent = React.createClass({

	getInitialState: function () {
		let feedbackInfo = {
			totalUsers : 100,
			fiveStars: 61,
			fourStars: 39
		};

		return {feedbackInfo};
	},

	render: function () {
		return (
			<div className={styles.FeedBackContainer}>
				<Header title="FEEDBACK"/>
				<div className={'cf'}>
					<div className={styles["cell"]}>
						<StarsComponent name="first" starCount = {5} percentage={100} text={[<span key="1" className={styles["percentage"]} >61%</span>, " gave 5/5 star rating"]}/>
					</div>
					<div className={styles["cell"]}>
						<StarsComponent name="second" starCount = {4} percentage={80} text={[<span key="2" className={styles["percentage"]} >39%</span>, " gave 4/5 star rating"]}/>
					</div>
				</div>
			</div>
		);
	}
});

export default FeedBackComponent;



