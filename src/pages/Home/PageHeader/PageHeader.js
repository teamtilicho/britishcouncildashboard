import React from 'react';
import ReactDOM from 'react-dom';
import styles from './PageHeader.css';
import cx from 'classnames'

var PageHeader = React.createClass({

    render: function () {
        return (
            <div className={cx(styles["header"], 'cf')}>
                <img alt="" src="http://apsaps.vassarlabs.com/images/logo.b91d895e.png"/>
                <p className={styles["heading-font"]}>APSCHE - British Council India Dashboard</p>
            </div>
        );
    }
});

export default  PageHeader;