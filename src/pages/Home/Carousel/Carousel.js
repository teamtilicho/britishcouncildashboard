import styles from './Carousel.css';
import React from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import Helpers from "../../../utils/Helpers";

let getNextSlideValue = (present, total) => {
	let k = present + 1;

	if (k === total) {
		k = 0;
	};

	return k;
};

let getPreviousSlideValue = (present, total) => {
	let k = present - 1;

	if (k < 0) {
		k = total - 1;
	}

	return k
}

let Slide = React.createClass({
	getInitialState(){
		return {
			left: this.props.left || "0px",
			zIndex: this.props.zIndex || 0,
			display: this.props.display || "none"
		}
	},
	componentWillReceiveProps(props){
		this.setState({
			left: "left" in props ? props.left : this.state.left,
			zIndex: "zIndex" in props ? props.zIndex : this.state.zIndex,
			display: "display" in props ? props.display : this.state.display
		});
	},
	render() {
		return <img src={this.props.src} alt={this.props.alt} className={styles["slide"]} style={{
			left: this.state.left,
			display: this.state.display,
			zIndex: this.state.zIndex
		}}/>;
	}
});

let Carousel = React.createClass({
	getInitialState(){
		return {
			active: 0,
			moveType: null,
			previous: null,
			slides: [],
			count: 0,
			width: "100%",
			height: 0,
			animationEnded: true,
			autoPlayTimer: null
		}
	},
	// componentWillMount(){
	// 	this.resizeHandler();
	// },
	componentDidMount(){
		let children = this.props.children;
		if(!Array.isArray(children)) {
			children = [children]
		}

		let slides = children.map(v => {
			if (v.type === "img" && "src" in v.props) {
				return {
					src: v.props.src,
					alt: v.props.alt
				};
			}
		});
console.log(this.props.height, this.props.width, ReactDOM.findDOMNode(this).offsetWidth,(this.props.height / this.props.width) * ReactDOM.findDOMNode(this).offsetWidth)
		this.setState({
			count: slides.length,
			slides: slides,
			width: ReactDOM.findDOMNode(this).offsetWidth,
			height: (this.props.height / this.props.width) * ReactDOM.findDOMNode(this).offsetWidth
		});

		Helpers.addEvent(window, "resize", this.resizeHandler);

		if (this.props.autoplay) {
			this.autoPlay();
		}
	},
	componentWillUnmount () {
		Helpers.removeEvent(window, "resize", this.resizeHandler);
		if (this.state.autoPlayTimer) {
			clearInterval(this.state.autoPlayTimer);
		}
	},
	resizeHandler(){
		let width = ReactDOM.findDOMNode(this).offsetWidth;

		this.setState({
			width: width,
			height: (this.props.height / this.props.width) * width
		});
	},
	showPrevious(e){
		e && e.preventDefault();

		this.setState({
			moveType: "prev",
			previous: this.state.active,
			animationEnded: false,
			active: getPreviousSlideValue(this.state.active, this.state.count)
		})
	},
	showNext(e){
		e && e.preventDefault();

		this.setState({
			moveType: "next",
			previous: this.state.active,
			animationEnded: false,
			active: getNextSlideValue(this.state.active, this.state.count)
		})
	},
	goToSlide(goto){
		if (goto !== this.state.active) {
			this.setState({
				animationEnded: false,
				moveType: "goto",
				previous: this.state.active,
				active: goto
			});
		}
	},
	onTransitionEnd(){
		this.setState({
			animationEnded: true
		})

		if (this.props.autoplay) {
			this.autoPlay();
		}
	},
	play: function(){
		this.setState({
			moveType: "next",
			previous: this.state.active,
			animationEnded: false,
			active: getNextSlideValue(this.state.active, this.state.count)
		});
	},
	autoPlay: function () {
		if (this.state.autoPlayTimer) {
			clearTimeout(this.state.autoPlayTimer);
		}
		if (this.props.autoplay) {
			this.setState({
				autoPlayTimer: setTimeout(this.play, this.props.autoplaySpeed)
			});
		}
	},
	render() {
		let prev = getPreviousSlideValue(this.state.active, this.state.count)
			,next = getNextSlideValue(this.state.active,this.state.count)
			,control_styles = {left: "0px", width: "100%", height: this.state.height + "px"};

		if (this.state.moveType !== null && this.state.animationEnded !== true) {
			control_styles.transform = "translateX(" + (this.state.previous < this.state.active ? "-" : "") + this.state.width + "px)";
			control_styles.transitionDuration = "500ms";
			control_styles.transitionTimingFunction = 'ease-in';
		}

		return <div className={styles.Carousel} ref="carousel_container">
			<div className={styles["slides-container"]} ref="carousel_slides_container" style={{width: "100%", height: this.state.height + "px"}}>
				<div className={styles["slides-control"]} style={control_styles} onTransitionEnd={this.onTransitionEnd}>
					{
						this.state.slides.map((v, k) => {
							let style = {};

							if (this.state.moveType === null) {
								if (k === next) {
									style.display = "block";
									style.left = this.state.width
								} else if (k === prev) {
									style.display = "block";
									style.left =  (-1 * this.state.width)
								} else if (k === this.state.active) {
									style.display = "block";
									style.zIndex = 10;
								}
							} else if (!this.state.animationEnded) {
								if (k === this.state.active) {
									style.left = (this.state.previous < this.state.active ? 1 : -1) * this.state.width + "px";
									style.zIndex = 10;
									style.display = "block";
								}

								if (this.state.moveType === "goto" && k !== this.state.active && k !== this.state.previous) {
									style.left = "0px";
									style.zIndex = 0;
									style.display = "none";
								}
							} else {
								if (k === this.state.active) {
									style.left = "0px";
								} else {
									style.left = "0px";
									style.zIndex = 0;
									style.display = "none";
								}
							}

							return <Slide key={k} src={v.src} alt={v.alt} {...style}/>
						})
					}
				</div>
			</div>
			<ul className={styles["pagination"]}>
				{
					this.state.slides.map((v, k) => {
						return <li className={styles["item"]} key={k}>
							<a href="#" className={cx(styles["atag"], {[styles["active"]]: k === this.state.active})} onClick={(e)=>{e.preventDefault();this.goToSlide(k);}}>1</a>
						</li>
					})
				}
			</ul>
		</div>;
	}
});

Carousel.defaultProps = {
  autoplay: true,
  autoplaySpeed: 5000,
  width: 940,
  height: 350
}


// let testCarusel = React.createClass({
// 	render() {
// 		let {currentSlide, slideCount, ...props} = this.props;

// 		return <Carousel  width="940" height="350">
// 			<img src="http://slidesjs.com/img/example-slide-350-1.jpg" alt="Photo by: Missy S Link: http://www.flickr.com/photos/listenmissy/5087404401/" />
// 			<img src="http://slidesjs.com/img/example-slide-350-2.jpg" alt="Photo by: Daniel Parks Link: http://www.flickr.com/photos/parksdh/5227623068/"/>
// 			<img src="http://slidesjs.com/img/example-slide-350-3.jpg" alt="Photo by: Mike Ranweiler Link: http://www.flickr.com/photos/27874907@N04/4833059991/"/>
// 			<img src="http://slidesjs.com/img/example-slide-350-4.jpg" alt="Photo by: Stuart SeegerLink: http://www.flickr.com/photos/stuseeger/97577796/"/>
// 		</Carousel>;
// 	}
// });

export default Carousel;