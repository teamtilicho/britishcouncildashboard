import React from 'react';
import Carousel from './Carousel';
import CircularProgressStatus from './CircularProgressStatus';
import Testimonials from './Testimonials';
import TimeLineComponent from './TimeLine'
import FeedBackComponent from './FeedBack'
import PageHeader from './PageHeader'
import PageFotter from './PageFotter'

class HomePage extends React.Component {
	render() {
		return <div>
			<PageHeader />
			<Carousel  width="800" height="289">
				<img src="assets/1.png" alt="" />
				<img src="assets/6.png" alt="" />
				<img src="assets/3.png" alt="" />
				<img src="assets/4.png" alt="" />
				<img src="assets/5.png" alt="" />
				<img src="assets/2.png" alt="" />
				<img src="assets/7.png" alt="" />
				<img src="assets/8.png" alt="" />
			</Carousel>
			<CircularProgressStatus/>
			<TimeLineComponent />
			<FeedBackComponent />
			<Testimonials />
			<PageFotter />
		</div>;
	}
};

export default HomePage;