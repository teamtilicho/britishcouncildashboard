/**
 * Created by Ravikumar on 23/06/17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../../../Components/Header';
import styles from './TimeLine.css';
import cx from 'classnames';

var timeLineInfoEnum = {
	done : "green",
	onGoing: "orange",
	idle: "gray"
};

var timeLineInfoDivDesignDirection = {
	up : "up",
	down : "down"
};

function calculateTheCurrentState() {

	for(var i =0; i < timeLineInfo.length;i++) {
		if (timeLineInfo[i].status == timeLineInfoEnum.onGoing) {
			return i;
		}
	}
	return 1;
}

var timeLineInfo = [
	{
		number: 1,
		text: "Master Teachers Training",
		status: timeLineInfoEnum.done,
		lineDirection : timeLineInfoDivDesignDirection.up
	},
	{
		number: 2,
		text: "Identification of 1 Lakh Students & 2500 Teachers",
		status: timeLineInfoEnum.onGoing,
		lineDirection : timeLineInfoDivDesignDirection.down
	},
	{
		number: 3,
		text: "Identification of Teachers Training Centres",
		lineDirection : timeLineInfoDivDesignDirection.up,
		status: timeLineInfoEnum.idle
	},
	{
		number: 4,
		text: "Teacher Training in July",
		status: timeLineInfoEnum.idle,
		lineDirection : timeLineInfoDivDesignDirection.down
	},
	{
		number: 5,
		text: "Student Training in August",
		status: timeLineInfoEnum.idle,
		lineDirection : timeLineInfoDivDesignDirection.up
	},
];

var TimeLineComponent = React.createClass({
	getInitialState(){
		return {
			status: 2
		}
	},
	render: function () {
		return (
			<div className={styles["TimeLine"]}>
				<Header title="TIMELINE"/>
				<div className={styles.container}>
					<div className={styles.step}>
						<div className={styles.text}>Master Teachers Training</div>
						<div className={styles.dot} style={{backgroundColor: this.state.status >= 1 ? '#7bb725' : '#4f4f4f'}}>
							<span>1</span>
						</div>
						<div className={cx(styles.bar, styles.right)} style={{borderColor: this.state.status > 1 ? '#7bb725' : 'gray'}}></div>
						<div className={cx(styles.pointer, styles.top)} style={{borderColor: this.state.status >= 1 ? '#7bb725' : 'gray'}}></div>
					</div>
					<div className={styles.step}>
						<div className={styles.dot} style={{backgroundColor: this.state.status >= 2 ? '#f09852' : '#4f4f4f'}}>
							<span>2</span>
						</div>
						<div className={styles.text}>Identification of 1 Lakh Students &amp; 2500 Teachers</div>
						<div className={cx(styles.bar, styles.left)} style={{borderColor: this.state.status > 1 ? '#7bb725' : 'gray'}}></div>
						<div className={cx(styles.bar, styles.right)} style={{borderColor: this.state.status > 2 ? '#f09852' : 'gray'}}></div>
						<div className={cx(styles.pointer, styles.bottom)} style={{borderColor: this.state.status >= 2 ? '#f09852' : 'gray'}}></div>
					</div>
					<div className={styles.step}>
						<div className={styles.text}>Identification of Teachers Training Centres</div>
						<div className={styles.dot} style={{backgroundColor: this.state.status >= 3 ? '#e74c3c' : '#4f4f4f'}}>
							<span>3</span>
						</div>
						<div className={cx(styles.bar, styles.left)} style={{borderColor: this.state.status > 2 ? '#e67e22' : 'gray'}}></div>
						<div className={cx(styles.bar, styles.right)} style={{borderColor: this.state.status > 2 ? '#e74c3c' : 'gray'}}></div>
						<div className={cx(styles.pointer, styles.top)} style={{borderColor: this.state.status >= 3? '#e74c3c' : 'gray'}}></div>
					</div>
					<div className={styles.step}>
						<div className={styles.dot} style={{backgroundColor: this.state.status >= 4 ? '#8e44ad' : '#4f4f4f'}}>
							<span>4</span>
						</div>
						<div className={styles.text}>Teacher Training in July</div>
						<div className={cx(styles.bar, styles.left)} style={{borderColor: this.state.status > 2 ? '#e74c3c' : 'gray'}}></div>
						<div className={cx(styles.bar, styles.right)} style={{borderColor: this.state.status > 2 ? '#8e44ad' : 'gray'}}></div>
						<div className={cx(styles.pointer, styles.bottom)} style={{borderColor: this.state.status >= 4 ? '#8e44ad' : 'gray'}}></div>
					</div>
					<div className={styles.step}>
						<div className={styles.text}>Student Training in August</div>
						<div className={styles.dot} style={{backgroundColor: this.state.status >= 5 ? '#2980b9' : '#4f4f4f'}}>
							<span>5</span>
						</div>
						<div className={cx(styles.bar, styles.left)} style={{borderColor: this.state.status > 2 ? '#8e44ad' : 'gray'}}></div>
						<div className={cx(styles.pointer, styles.top)} style={{borderColor: this.state.status >= 5 ? '#2980b9' : 'gray'}}></div>
					</div>
				</div>
			</div>
		);
	}
});

//export default TimeLineComponent;
module.exports = TimeLineComponent;