import React from 'react';



var FeedBackForm = React.createClass({

	getInitialState : function () {
		return {
			totalUsers : 0,
			fiveStars : 0,
			fourStars: 0
		}
    },

	perpareInfo : function () {
		return 	{
            totalUsers : this.state.totalUsers,
            fiveStars : this.state.fiveStars,
            fourStars: this.state.fourStars
        }
    },

    onFormFill: function () {
        this.props.onFill(this.perpareInfo);
    },

    handleFourStarRating: function (e) {

        var oldInfo = this.state.info;

        this.setState({
            fourStars: e.target.value
        });
        this.props.feedBackChanged(this.state);
    },

    handleTotalNumber: function (e) {
		//TODO : Check if 4 + 5 < e.value
        this.setState({
            totalUsers: e.target.value
        });
        this.props.feedBackChanged(this.state);
    },

    handleFiveStarRating: function (e) {

        var oldInfo = this.state.info;

        this.setState({
			fiveStars: e.target.value
        });
        this.props.feedBackChanged(this.state);
    },

	render: function () {
		return (

        	<div>
				<h1>{this.props.title}</h1>
				<label>5 Stars:
				<input type="text" name="fiveStars" value={this.state.fiveStars} onChange={this.handleFiveStarRating} />
				</label>
				<label>4 Stars:
				<input type="text" name="fourStars" value={this.state.fourStars} onChange={this.handleFourStarRating} />
				</label>
				<label>Total:
				<input type="text" name="total" value={this.state.totalUsers} onChange={this.handleTotalNumber} />
				</label>
			</div>

		);
    }
})

var ChildForm = React.createClass({

    getInitialState : function () {
        return {
            info : {
                registered : this.props.info.registered,
                total : this.props.info.total
            },
        }
    },

    handleTotalNumber: function (e) {

        var oldInfo = this.state.info;

        this.setState({
            info : {
                registered : e.target.value,
                total : oldInfo.total
            }
        });
        this.props.onFormChange(this.state.info);
    },

    handleRegisteredNumber: function (e) {

    	var oldInfo = this.state.info;

    	this.setState({
			info : {
				registered : e.target.value,
				total : oldInfo.total
			}
		});
        this.props.onFormChange(this.state.info);
    },

	render: function () {

		return (
			<div>
				<h1>{this.props.title}</h1>
				<label>Registered:
				<input type="text" name="registered" value={this.state.info.registered} onChange={this.handleRegisteredNumber} />
				</label>
				<label>Total:
				<input type="text" name="total" value={this.state.info.total} onChange={this.handleTotalNumber} />
				</label>
			</div>
		);
    }
});

var MainForm = React.createClass({

    getInitialState : function () {
        return {
        	studentsInfo : {
            	registered : 0,
                total : 100000
			},
            teachersInfo :{
				registered : 0,
            	total : 2500
			},
            mastersInfo : {
                registered: 0,
                total: 114
            }
        }
    },

    onSubmit : function () {
        return 	{
            studentsInfo : this.state.studentsInfo,
            teachersInfo : this.state.teachersInfo,
            mastersInfo: this.state.mastersInfo
        }
    },

	setStudentsInfo: function (info) {
		var oldInfo = this.state.studentsInfo;
        oldInfo.registered = info.registered;
        oldInfo.total = info.total
		this.setState({
			studentsInfo: oldInfo
		});
        this.props.formChanged(this.state);
    },
    setTeachersInfo: function (info) {
        var oldInfo = this.state.teachersInfo;
        oldInfo.registered = info.registered;
        oldInfo.total = info.total
        this.setState({
            teachersInfo: oldInfo
        });
        this.props.formChanged(this.state);
    },
    setMastersInfo: function (info) {
        var oldInfo = this.state.mastersInfo;
        oldInfo.registered = info.registered;
        oldInfo.total = info.total
        this.setState({
            mastersInfo: oldInfo
        });
        this.props.formChanged(this.state);
    },
    render: function () {
        return (
			<div>
				<ChildForm title="Students Info" info={this.state.studentsInfo} onFormChange={this.setStudentsInfo}/>
				<ChildForm title="Teachers Info" info={this.state.teachersInfo} onFormChange={this.setTeachersInfo}/>
				<ChildForm title="Masters Info" info={this.state.mastersInfo} onFormChange={this.setMastersInfo}/>
			</div>
        );
    }
})

var TimeLineForm = React.createClass({

    getInitialState : function () {
        return {
            totalUsers : 0,
            fiveStars : 0,
            fourStars: 0
        }
    },

    onSubmit : function () {
        return 	{
            totalUsers : this.state.totalUsers,
            fiveStars : this.state.fiveStars,
            fourStars: this.state.fourStars
        }
    },

    render: function () {
        return (
			<div>
				<form action="/action_page.php">
					<p>First name:</p>
					<input type="text" name="firstname" value="Mickey" />
					<p>Last name:</p>
					<input type="text" name="lastname" value="Mouse" />
					<input type="submit" value="Submit" />
				</form>
			</div>
        );
    }
})

class AdminPage extends React.Component {

	getInitialState() {
		return {

            studentsInfo : {
                registered : 0,
                total : 100000
            },
            teachersInfo :{
                registered : 0,
                total : 2500
            },
            mastersInfo : {
                registered: 0,
                total: 114
            },
            totalUsers : 0,
            fiveStars : 0,
            fourStars: 0
		}
	}

	onFormFill() {
		//TODO : Implementation needed
	}

	formChanged(state) {

        this.setState({

			studentsInfo: state.studentsInfo,
            teachersInfo: state.teachersInfo,
            mastersInfo: state.mastersInfo,
		});
	}

	feedBackChanged(feedBackInfo){
        this.setState({

            totalUsers: feedBackInfo.totalUsers,
            fiveStars: feedBackInfo.fiveStars,
            fourStars: feedBackInfo.fourStars,
        });
	}

	render() {
		return (
			<div>
				<form onSubmit={this.onFormFill}>
					<MainForm formChanged={this.formChanged}/>
					<FeedBackForm title ="FeedBack" feedBackChanged={this.feedBackChanged} />
					<input type="submit" value="Submit" />
				</form>
			</div>
		);
	}
};

export default AdminPage;