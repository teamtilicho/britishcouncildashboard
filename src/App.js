import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CircularProgressbar from 'react-circular-progressbar';
import Stepper from 'react-stepper-horizontal';
import StarRatingComponent from 'react-star-rating-component';

var Carousel = require('nuka-carousel');

class App extends Component {

  render() {
    return (
      <div className="App">
          <FirstComponent />
          <HeaderComponent title="STATS" />
          <SecondComponent />
          <div className="CompleteTimeLineDiv">
          <HeaderComponent title="TIMELINE" class="GrayHeader"/>
          <TimeLineComponent />
          </div>
          <HeaderComponent title="FEEDBACK"/>
          <FeedBackComponent />
          <HeaderComponent title="REVIEWS" class="GrayHeader"/>
          <ReviewCarousel />
      </div>
    );
  }
}

var HeaderComponent = React.createClass({

    getDefaultProps: function () {
        return {
            class: 'HeaderClass'
        };
    },
    render: function () {
        return(
            <div className={this.props.class}>
                <h1>{this.props.title}</h1>
                <div className="UnderLine" />
            </div>);
    }
});

var timeLineInfoEnum = {
    done : "green",
    onGoing: "orange",
    idle: "gray"
};

var timeLineInfoDivDesignDirection = {
    up : "up",
    down : "down"
};

function calculateTheCurrentState() {

    for(var i =0; i < timeLineInfo.length;i++) {
        if (timeLineInfo[i].status == timeLineInfoEnum.onGoing) {
            return i;
        }
    }
    return 1;
}

var timeLineInfo = [
    {
        number: 1,
        text: "Master Teachers Training",
        status: timeLineInfoEnum.done,
        lineDirection : timeLineInfoDivDesignDirection.up
    },
    {
        number: 2,
        text: "Identification of 1 Lakh Students & 2500 Teachers",
        status: timeLineInfoEnum.onGoing,
        lineDirection : timeLineInfoDivDesignDirection.down
    },
    {
        number: 3,
        text: "Identification of Teachers Training Centres",
        lineDirection : timeLineInfoDivDesignDirection.up,
        status: timeLineInfoEnum.idle
    },
    {
        number: 4,
        text: "Teacher Training in July",
        status: timeLineInfoEnum.idle,
        lineDirection : timeLineInfoDivDesignDirection.down
    },
    {
        number: 5,
        text: "Student Training in August",
        status: timeLineInfoEnum.idle,
        lineDirection : timeLineInfoDivDesignDirection.up
    },
];

var studentsInfo = {
    total : 100000,
    registered: 25000,
    text: "Students identified in colleges across the state",
    id: 1
};

var teachersInfo = {
    total : 2500,
    registered: 1200,
    text: "Teachers identified in colleges across the state",
    id: 2
};

var mastersInfo = {
    total : 114,
    registered: 114,
    text: "Master trainers trained in SPMVV, Tirupati & ANU, Guntur",
    id: 3
};

var CircleRepresentation = React.createClass({

    render: function() {
        var percentage = (this.props.info.registered/this.props.info.total) * 100;
        var id = this.props.info.id;
        return (
            <div className="CircularProgressDiv">
                 <svg height={100} width={100} >
                    <CircularProgressbar percentage={percentage} strokeWidth="10" initialAnimation={true}/>
                 </svg>
                 <p>{this.props.info.registered + "/" + this.props.info.total}</p>
                 <p>{this.props.info.text}</p>
            </div>
        );
    }
});


var SecondComponent = React.createClass({

    getInitialState : function() {

        return {
            studentsInfo: studentsInfo,
            teachersInfo : teachersInfo,
            mastersInfo : mastersInfo
        };
    },

    render:function() {
        return (
            <div className="SecondComponentDiv">
                <CircleRepresentation info={this.state.studentsInfo}/>
                <CircleRepresentation info={this.state.teachersInfo}/>
                <CircleRepresentation info={this.state.mastersInfo} />
            </div>
        );
    }
});

var feedbackInfo = {

    totalUsers : 10000,
    fiveStars: 4000,
    fourStars: 3000
};


var FeedBackComponent = React.createClass({

    getInitialState: function () {
        return {feedbackInfo: feedbackInfo};
    },

    render: function () {

        return (
            <div className="FeedBackContainer">
                <div className="FeedBackContainerLeft">
                    <StarsComponent starCount = {5} percentage={(this.state.feedbackInfo.fiveStars/this.state.feedbackInfo.totalUsers) * 100}/>
                </div>
                <div className="FeedBackContainerRight">
                    <StarsComponent starCount = {4} percentage={(this.state.feedbackInfo.fourStars/this.state.feedbackInfo.totalUsers) * 100} />
                </div>
            </div>
        );
    }
});

var StarsComponent = React.createClass({

    render: function () {
        return (
        <div>
            <StarRatingComponent
            starCount = {this.props.starCount}
            value = {this.props.starCount}
            className="Star"
            />
            <p>{this.props.percentage + "%" + " gave " + this.props.starCount + "/5 star rating"} </p>
        </div>
            );
    }
});


var TimeLineComponent = React.createClass({

    render: function () {

        return (
            <div className="TimeLine">
                <Stepper steps={ [{title: timeLineInfo[0].text}, {title: timeLineInfo[1].text}, {title: timeLineInfo[2].text}, {title: timeLineInfo[3].text}, {title: timeLineInfo[4].text}] }
                         activeStep={calculateTheCurrentState()}
                         activeColor="orange"
                         completeColor="green"
                         defaultColor="black"
                         defaultBarColor="gray"
                         completeBarColor="green"
                />
            </div>
        );
    }
});

var FirstComponent = React.createClass({
    mixins : [Carousel.ControllerMixin],
    render() {
        return (
            <Carousel autoplay={true} wrapAround={true}>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide1"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide2"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide3"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide4"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide5"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide6"/>
            </Carousel>
        );
    }
});

var ReviewCarousel = React.createClass({
    mixins: [Carousel.ControllerMixin],
    render:function () {

        return (
            <div className="Review">
            <Carousel autoplay={true} wrapAround={true} >
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide1"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide2"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide3"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide4"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide5"/>
                <img src="http://placehold.it/1000x400/ffffff/c0392b/&text=slide6"/>
            </Carousel>
            </div>
        );
    }

});


export default App;
