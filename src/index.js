import React from 'react';
import {render} from 'react-dom';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import './styles.css';

import HomePage from "./pages/Home";
import AdminPage from "./pages/Admin";

let PageNotFound = () => {
	return <div>
		<h1>Page Not Found.</h1>
		<p>Go to <Link to="/">Home Page</Link></p>
	</div>;
}

render(
	<Router basename="britishcouncil">
		<Switch>
			<Route path="/" exact component={HomePage}/>
			<Route path="/admin" component={AdminPage} />
			<Route component={PageNotFound} />
		</Switch>
	</Router>
, document.getElementById('app')
);