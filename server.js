var express = require('express'),
	app = express(),
	http = require('http'),
	fs = require('fs'),
	port = process.env.PORT || 4002,
	webpack = require('webpack'),
	webpackMiddleware = require('webpack-dev-middleware'),
	ProgressPlugin = require('webpack/lib/ProgressPlugin'),
	bodyParser = require('body-parser'),
	path = require('path');

app.use(bodyParser.json());

app.use('/build', express.static('build'));

app.use('/assets', express.static('assets'));

app.use('/js', express.static('js'));

app.use(function(req, res, next) {
  //console.log('Request URL:', req.originalUrl);
  next();
},webpackMiddleware(webpack(require('./webpack.config.js')), {
    noInfo: false,
    quiet: false,
    lazy: false,
    watchOptions: {
        aggregateTimeout: 300,
        poll: true
    },
    publicPath: "/public/",
    stats: {
        colors: true,
		hash: true,
		timings: true,
		assets: true,
		chunks: false,
		chunkModules: false,
		modules: false,
		children: false
    }
}));

app.all(['/*','/*/*','/*/*/*','/*/*/*/*'], function(req, res) {
	res.end(fs.readFileSync( path.join(__dirname,'/index.html') ));
});

http.createServer(app).listen(port, function(err) {
	if(err) throw err;
	console.log(' Express server listening on port - ' + port);
});
