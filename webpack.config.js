var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var ManifestPlugin = require('webpack-manifest-plugin');

module.exports = {
	devtool: debug ? "#source-map" : false,
	entry: ["babel-polyfill","./src/index.js"],
	module: {
		loaders: [
			{
				test: /\.js$/,
				use: 'babel-loader'
			},
			{
				test: /\.css$/,
				use: debug ? [
					'style-loader',
					'css-loader?modules&importLoaders=1&localIdentName=[path][name]_[local]_[hash:base64:6]',
					'postcss-loader?sourceMap=inline'
				] : ExtractTextPlugin.extract({ fallbackLoader: 'style-loader', loader:'css-loader?modules&importLoaders=1!postcss-loader?sourceMap=none'})
			},
			{
				test : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
				use : 'file-loader'
			},
			{
				test : /\.(jpe?g|png|gif|svg)$/i,
				use : 'file-loader'
			}
		]
	},
	resolve: {
		modules: [path.resolve(__dirname, "node_modules"), "node_modules"],
		extensions: ['.js', '.css', '.scss']
	},
	output: {
		path: debug ? __dirname + "/public" :  path.resolve(__dirname, 'build'),
		filename: debug ? "build.min.js" : 'build.[hash].js'
	},
	plugins: debug ? 
		[
			new webpack.DefinePlugin({
				'NODE_ENV': '"development"'
			}),
			new webpack.LoaderOptionsPlugin({
				minimize: true,
				debug: false,
				context: __dirname,
				options: {
					postcss: [
						require('postcss-import'),
						require('postcss-cssnext'),
						require('postcss-simple-vars'),
						require('postcss-nested')
					]
				}
			})
		]
		: [
			new webpack.LoaderOptionsPlugin({
				minimize: true,
				debug: false,
				context: __dirname,
				options: {
					postcss: [
						require('postcss-import'),
						require('postcss-cssnext'),
						require('postcss-simple-vars'),
						require('postcss-nested')
					]
				}
			}),
			new webpack.DefinePlugin({
				'NODE_ENV': '"production"'
			}),
			new ExtractTextPlugin({
				filename: "build.[hash].css",
				allChunks: true
			}),
			new ManifestPlugin({fileName: 'build.json'})
		]
};
